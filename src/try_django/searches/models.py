from django.db import models
from django.conf import settings


class SearchQuery(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, blank=True, null=True)
    query = models.CharField(max_length=220)
    timestamp = models.DateTimeField(auto_now_add=True)