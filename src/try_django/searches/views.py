from django.shortcuts import render
from django.http import Http404, HttpResponseRedirect, HttpResponse
from .models import SearchQuery
from blog.models import BlogPost
from blog.views import generate_paginationbar_pages
import math


def search_view(request, *args, **kwargs):
    q = request.GET.get('q', None)
    user = None
    qs = BlogPost.objects.none()
    if request.user.is_authenticated:
        user = request.user

    if q is not None:
        SearchQuery.objects.create(user=user, query=q)
        qs = BlogPost.objects.search(query=q)
        if request.user.is_superuser:
            qs = BlogPost.objects.superuser_search(query=q)
        elif request.user.is_authenticated:
            user_blog_list = BlogPost.objects.filter(user=request.user)
            qs = (qs | user_blog_list).distinct()

    per_page = 3
    page = 1  # pages counting from 1
    try:
        page = kwargs['page']
    except KeyError:
        return HttpResponseRedirect("1/?q={}".format(q))

    number_of_blog_posts = len(qs)
    number_of_pages = math.ceil(number_of_blog_posts / per_page)
    ix_down = (page - 1) * per_page
    ix_up = min(ix_down + per_page, number_of_blog_posts)
    if ix_down > number_of_blog_posts:
        raise Http404("Exceed number of blog_posts.")
    qs = qs[ix_down: ix_up]

    template_name = "searches/view.html"
    pages = generate_paginationbar_pages(number_of_pages, page)
    context = {"object_list": qs, "pages": pages, "page": page, "last_page": number_of_pages,
               "query": q, "no_results": number_of_blog_posts}
    return render(request, template_name, context)


def tmp_view(request, *args, **kwargs):
    return HttpResponse("page exist")