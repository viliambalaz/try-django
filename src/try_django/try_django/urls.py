"""try_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.urls import path, re_path, include
from .views import *
from blog.views import blog_post_create_view
from searches.views import search_view, tmp_view

from django.conf.urls import url
from django.contrib.sitemaps.views import sitemap
from blog.sitemap import BlogPostSitemap

from django.conf.urls.i18n import i18n_patterns
from django.utils.translation import ugettext_lazy as _


sitemaps = {
    'blogpostsitemap': BlogPostSitemap
}

urlpatterns = [
    url(r'^sitemap[.]xml$', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    path('blog-new/', blog_post_create_view, name="blog_create"),
    path('blog-new/<slug:title>/', blog_post_create_view, name="blog_create"),
    #path('blog/', include('blog.urls')),

    #path('', home_page, name="home_page"),
    re_path(r'^about/$', about_page, name="about_page"),
    path('contact/', contact_page, name="contact_page"),
    path('example/', example_page, name="example_page"),

    path('search/', search_view, name="search_view"),
    path('search/<int:page>/', search_view, name="search_page"),


    path('accounts/', include('django.contrib.auth.urls'), name="accounts_page"),
    path('admin/', admin.site.urls),
]

urlpatterns += i18n_patterns(#u'',
    url(r'^$', home_page, name='homepage'),
    path(r'blog/', include('blog.urls')),
    #todo: add blogpost urls
)


if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
