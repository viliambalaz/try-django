from django.http import HttpResponse
from django.shortcuts import render
from django.template.loader import get_template
from .forms import ContactForm
from blog.models import BlogPost
import random


def home_page(request, *args, **kwargs):
    my_title = "Home page"
    qs = BlogPost.objects.all().published()
    obj = None
    if qs:
        ix = random.randint(0, len(qs)-1)
        obj = qs[ix]

    context = {
        "title": my_title,
        "object": obj
    }
    return render(request, "home.html", context)


def contact_page(request, *args, **kwargs):

    if request.POST:
        print(request.POST)
        print("Hello POST method from contact.html")

    form = ContactForm(request.POST or None)
    if form.is_valid():
        print('------------form is valid ----------')
        print(form.cleaned_data)
        form = ContactForm()

    print("valid form: ", form.is_valid())
    my_title = "Contact us"
    context = {"title": my_title, "form": form}
    return render(request, "form.html", context)


def about_page(request, *args, **kwargs):
    my_title = "About us"
    context = {"title": my_title}
    return render(request, "about.html", context)


def example_page(request, *args, **kwargs):
    my_title = "Example"
    context = {"title": my_title}

    template_name = "hello_world.html"
    template_obj = get_template(template_name)
    rendered_item = template_obj.render(context)
    #print("rendered item: {}".format(rendered_item))
    return HttpResponse(rendered_item)


