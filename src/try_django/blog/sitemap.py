from django.contrib.sitemaps import Sitemap
from .models import BlogPost

class BlogPostSitemap(Sitemap):
    changefreq = "monthly"
    priority = 0.3
    i18n = True

    def items(self):
        return BlogPost.objects.filter()

    def lastmod(self, blogpost):
        return blogpost.publish_date