# Generated by Django 2.2.2 on 2019-06-24 20:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0014_comment'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='comment',
            options={'ordering': ['timestamp']},
        ),
        migrations.AddField(
            model_name='comment',
            name='last_updated',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
