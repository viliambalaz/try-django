# Generated by Django 2.2.2 on 2019-06-19 20:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0011_userprofile'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='avatar',
            field=models.ImageField(default='image/users_avatar/user.png', upload_to='image/users_avatars'),
        ),
    ]
