# Generated by Django 2.2.2 on 2019-06-19 21:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0012_auto_20190619_2059'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='avatar',
            field=models.ImageField(default='image/users_avatars/user.png', upload_to='image/users_avatars'),
        ),
    ]
