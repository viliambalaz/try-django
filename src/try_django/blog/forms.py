from django import forms
from .models import BlogPost
from django.template.defaultfilters import slugify


class BlogPostForm(forms.Form):
    title = forms.CharField()
    slug = forms.SlugField()
    content = forms.CharField(widget=forms.Textarea)


class BlogPostModelForm(forms.ModelForm):

    class Meta:
        model = BlogPost
        fields = ['title', 'image', 'content', 'publish_date']

    def clean_title(self):
        title = self.cleaned_data.get('title')
        slug_title = slugify(title)
        qs = BlogPost.objects.filter(slug=slug_title)
        if self.instance is not None:
            qs = qs.exclude(pk=self.instance.pk)
        if qs.exists():
            raise forms.ValidationError("Post with title '{}' already exists.".format(title))
        return title


class CommentForm(forms.Form):
    content = forms.CharField(widget=forms.Textarea(attrs={'cols': 80, 'rows': 4}), label='')
