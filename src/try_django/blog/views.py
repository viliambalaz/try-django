from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from .models import BlogPost, Comment
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404, get_list_or_404, redirect
from .forms import BlogPostModelForm, CommentForm
from django.utils import timezone
from django.db import IntegrityError
import math
# Create your views here.


def blog_post_list_view(request, *args, **kwargs):

    qs = BlogPost.objects.all().published()
    if request.user.is_superuser:
        qs = BlogPost.objects.all()
    elif request.user.is_authenticated:
        user_posts = BlogPost.objects.filter(user=request.user)
        qs = (qs | user_posts).distinct()

    # todo: user choice blog+post per page
    print(kwargs)
    per_page = 3
    page = 1  # pages counting from 1
    try:
        page = kwargs['page']
    except KeyError:
        page = 1

    number_of_blog_posts = len(qs)
    number_of_pages = math.ceil(number_of_blog_posts / per_page)
    ix_down = (page-1) * per_page
    ix_up = min(ix_down + per_page, number_of_blog_posts)
    if ix_down > number_of_blog_posts:
        raise Http404("Exceed number of blog_posts.")

    template_name = "blog/list.html"
    pages = generate_paginationbar_pages(number_of_pages, page)
    context = {"object_list": qs[ix_down: ix_up], "pages": pages, "page": page, "last_page": number_of_pages}

    return render(request, template_name, context)


#@login_required(login_url='/login')
@staff_member_required
def blog_post_create_view(request, *args, **kwargs):
    # create view
    template_name = "form.html"
    print('-------------------------------')
    print("files: ", request.FILES)
    print("request: ", request)

    try:
        title = kwargs['title']
    except KeyError:
        title = ""

    unsluged_title = unslugify(title)
    form = BlogPostModelForm(request.POST or None, request.FILES or None, initial={'title': unsluged_title})
    context = {"form": form}

    if form.is_valid():
        # to manipulate with form data
        obj = form.save(commit=False)
        obj.title = form.cleaned_data.get("title")
        obj.user = request.user
        obj.save()
        #obj = BlogPost.objects.create(**form.cleaned_data)
        form = BlogPostModelForm()
        context["save"] = True
        context["object"] = obj
        # todo: add message_list

    context["form"] = form
    return render(request, template_name, context)


def blog_post_detail_view(request, *args, **kwargs):

    try:
        obj = BlogPost.objects.get(slug=kwargs['title'])
    except BlogPost.DoesNotExist:
        return render(request, 'blog/create_new.html', {"obj_title": kwargs['title']})

    template_name = "blog/detail.html"
    context = {"object": obj}

    form = CommentForm(request.POST or None)
    context["comment_form"] = form

    if request.method == "POST":
        print(request)
        print(request.POST)

        if 'delete_yes' in request.POST:
            #print('I want to delete object ', obj.slug)
            obj_title = obj.slug
            obj.delete()
            return render(request, 'blog/create_new.html', {"obj_title": obj_title, "delete_yes": True})
        elif 'delete_cancel' in request.POST:
            #print('I do not want delete object ', obj.slug)
            return render(request, template_name, context)
        elif 'comment':
            if form.is_valid():
                Comment.objects.create(user=request.user, blogpost=obj, content=form.cleaned_data.get('content'))
                form = CommentForm()
                context["comment_form"] = form
                context["comment_save"] = True
                return render(request, template_name, context)

        return HttpResponseRedirect(obj.get_absolute_url())

    return render(request, template_name, context)


@staff_member_required
def blog_post_update_view(request, *args, **kwargs):

    obj = get_object_or_404(BlogPost, slug=kwargs['title'])
    form = BlogPostModelForm(request.POST or None, request.FILES or None, instance=obj)
    context = {}

    print(form.changed_data)
    print(form.is_valid())
    if form.is_valid() and form.changed_data != []:
        form.save()
        context["save"] = True
        if kwargs['title'] != obj.slug:
            return HttpResponseRedirect(obj.get_edit_url())

    template_name = "blog/form.html"
    context["form"] = form
    context["object"] = obj
    print(context)
    return render(request, template_name, context)


@staff_member_required
def blog_post_delete_view(request, *args, **kwargs):
    obj = get_object_or_404(BlogPost, slug=kwargs['title'])
    template_name = "blog/delete.html"
    context = {"object": obj}
    if request.method == "POST":
        print("request POST in delete")
        obj.delete()
        return blog_post_list_view(request, args, kwargs)
    return render(request, template_name, context)


def generate_paginationbar_pages(n: int, ix: int):
    " returns list of page numbers which displays in paginationbar"
    # maximum 11 pages(<li>) exclusive (Next, Previous) per web-page
    # ix - current page
    # -1 stands for <li>...</li>
    pages = [1, 2] + [-1] + [ix-2, ix-1, ix, ix+1, ix+2] + [-1] + [n-1, n]
    if n <= 11:
        return range(1, n+1)
    if ix <= 6:
        pages = [1, 2, 3, 4, 5, 6, 7, 8] + [-1] + [n-1, n]
    if n - ix <= 5:
        pages = [1, 2] + [-1] + [n-7, n-6, n-5, n-4, n-3, n-2, n-1, n]

    return pages



def unslugify(slug: str):
    tmp = slug.replace('-', ' ')
    return tmp.title()

