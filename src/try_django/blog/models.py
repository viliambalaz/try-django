from django.db import models
from django.conf import settings
# Create your models here.
from django.template.defaultfilters import slugify
from django.db.models.signals import pre_save
from django.utils import timezone
from django.db.models import Q

User = settings.AUTH_USER_MODEL


class BlogPostQuerySet(models.QuerySet):

    def published(self):
        now = timezone.now()
        return self.filter(publish_date__lte=now)

    def search(self, query):
        #return self.filter(content__contains=query)
        lookup = (Q(title__icontains=query) | Q(content__icontains=query) | Q(user__username__icontains=query))
        return self.filter(lookup)


class BlogPostManager(models.Manager):

    def published(self):
        #return self.get_queryset().published()
        now = timezone.now()
        return self.get_queryset().filter(publish_date__lte=now)

    def get_queryset(self):
        return BlogPostQuerySet(self.model, using=self._db)

    def search(self, query=None):
        if query is None:
            return self.get_queryset().none()
        return self.get_queryset().published().search(query)

    def superuser_search(self, query):
        if query is None:
            return BlogPost.objects.none()
        return self.get_queryset().search(query)



class BlogPost(models.Model):
    user = models.ForeignKey(User, default=1, null=True, on_delete=models.SET_NULL)  # user that created BlogPost
                                                                                    # by deleting set user to default
    image = models.ImageField(upload_to='image/', blank=True, null=True)
    # todo: validation title - cannot start with number
    title = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)
    content = models.TextField(null=True, blank=True)
    publish_date = models.DateTimeField(auto_now=False, auto_now_add=False, null=True, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)

    objects = BlogPostManager()

    class Meta:
        ordering = ['-publish_date', '-last_updated', '-timestamp']

    def get_slug(self):
        return slugify(self.title)

    def save(self, *args, **kwargs):
        self.slug = self.get_slug()
        if len(BlogPost.objects.filter(slug=self.slug)) >= 1:
            print("cannot save, unique slug, title already exist, ...")
            # raise exception or do something
            # return
        super(BlogPost, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return "/blog/{}/".format(self.slug)

    def get_edit_url(self):
        return "/blog/{}/edit/".format(self.slug)

    def get_delete_url(self):
        return "/blog/{}/delete".format(self.slug)

    def __str__(self):
        return "{} ({})".format(self.slug, self.id)

    def is_publish_recently(self) -> bool:
        if self.publish_date is None:
            return False
        return self.publish_date <= timezone.now()


def do_whatever_you_need_with(bp: BlogPost):
    print("hello pre_save Blog Post " + str(bp.id) + " " + bp.title)
    bp.slug = slugify(bp.title)


def extraInitForMyModel(**kwargs):
   instance = kwargs.get('instance')
   do_whatever_you_need_with(instance)

# pre_save.connect(extraInitForMyModel, BlogPost)


from django.contrib.auth.models import User
from django.db import models
from  django.db.models.signals import post_save


class UserProfile(models.Model):
    user   = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(upload_to='image/users_avatars', default="image/users_avatars/user.png")


def create_user_profile(sender, instance, created, **kwargs):
    if created:
       profile, created = UserProfile.objects.get_or_create(user=instance)

# This will create a userprofile each time a user is saved if it is created.
post_save.connect(create_user_profile, sender=User)


class Comment(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)
    blogpost = models.ForeignKey(BlogPost, on_delete=models.CASCADE)
    content = models.TextField()

    class Meta:
        ordering = ['timestamp']