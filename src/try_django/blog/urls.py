from django.contrib import admin
from django.urls import path, re_path
from .views import (blog_post_detail_view, blog_post_list_view,
                    blog_post_create_view, blog_post_update_view, blog_post_delete_view)

urlpatterns = [
    path('', blog_post_list_view, name="blog_list"),
    path('<int:page>/', blog_post_list_view, name="blog_page"),
    path('<slug:title>/', blog_post_detail_view, name="blog_detail"),
    path('<slug:title>/edit/', blog_post_update_view, name="blog_update"),
    path('<slug:title>/delete/', blog_post_delete_view, name="blog_delete"),

    path('blog-new/', blog_post_create_view, name="blog_create"),

]
