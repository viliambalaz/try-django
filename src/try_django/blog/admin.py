from django.contrib import admin
from .models import BlogPost, UserProfile, Comment
from django.utils import timezone


class BlogPostAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'slug', 'user', 'publish_date', 'is_publish_recently')
    readonly_fields = ['slug', 'last_updated', 'timestamp']

    def is_publish_recently(self, obj: BlogPost) -> bool:
        if obj.publish_date is None:
            return False
        return obj.publish_date <= timezone.now()
    is_publish_recently.short_description = "published"
    is_publish_recently.boolean = True


class CommentAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'timestamp', 'blogpost')


admin.site.register(BlogPost, BlogPostAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(UserProfile)

